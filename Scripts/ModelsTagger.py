import bpy
from bpy.props import EnumProperty
from bpy.types import Operator, Panel
from bpy.utils import register_class, unregister_class
 
 
 
# -------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------
 
 
class CityGMLPanel(Panel):
    bl_idname = 'CityGMLPanel'
    bl_label = 'City GML Tag Panel'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'City GML Tools'
    
 
    def draw(self, context):
        layout = self.layout
        layout.row()
        layout.label(text="City GML config project tools")
        layout.row()
        layout.label(text="--------------------------------------")
        layout.row() 
        layout.operator('jason.tags', text='Init Building sample').action = 'INIT'
        layout.row()
        layout.label(text="--------------------------------------")
        layout.row()
        layout.label(text="Object mesh seperate")
        layout.row() 
        layout.operator('jason.tags', text='Co Planer and Seperate').action = 'INIT_WORLD'
        layout.row() 
        layout.operator('jason.tags', text='Normal and loos parts').action = 'TAG'
        layout.row()
        layout.operator('jason.tags', text='Seperate Roof').action = 'EMPTY'
        layout.row()
        layout.operator('jason.tags', text='Fill windows verts').action = 'FW'
        layout.label(text="--------------------------------------")
        layout.row()
        layout.label(text="Name Tags")
        layout.row()
        layout.operator('jason.tags', text='Tag Roof').action = 'R'
        layout.row()
        layout.operator('jason.tags', text='Tag Wall').action = 'W'
        layout.row()
        layout.operator('jason.tags', text='Tag Door').action = 'D'
        layout.row()
        layout.operator('jason.tags', text='Tag Window').action = 'WI'
        layout.row()
        layout.operator('jason.tags', text='Tag Ground').action = 'G'
        layout.row()
        layout.operator('jason.tags', text='Tag Tunnel').action = 'T'
        layout.row()
        layout.operator('jason.tags', text='Tag Tunnel Opening').action = 'TO'
        layout.row()
        layout.operator('jason.tags', text='Tag Indoor').action = 'IN'
        layout.row()
        layout.label(text="--------------------------------------")
        layout.row()
        layout.label(text="Set Generic Settings")
        layout.row()
        layout.operator('jason.tags', text='set Parent').action = 'P'
        layout.row()
        layout.operator('jason.tags', text='Set Hirarchy for children').action = 'H'
        layout.row()
        layout.operator('jason.tags', text='Clear Hirarchy for children').action = 'S'
        layout.label(text="--------------------------------------")
        layout.row()
        layout.operator('jason.tags', text='selection to cursor').action = 'CC'
# -------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------
 
 
 
class CityGML_Operators(Operator):
    bl_idname = 'jason.tags'
    bl_label = 'Json Tags'
    bl_description = 'Json Tags'
    bl_options = {'REGISTER', 'UNDO'}
 
    action: EnumProperty(
        items=[
            ('INIT_WORLD', 'init world', 'init world'),
            ('TAG', 'tag', 'tag'),
            ('W', 'w', 'w'),
            ('D', 'd', 'd'),
            ('R', 'r', 'r'),
            ('WI', 'wi', 'wi'),
            ('H', 'h', 'h'),
            ('P', 'p', 'p'),
            ('S', 's', 's'),
            ('G', 'g', 'g'),
            ('T', 't', 't'),
            ('TO', 'to', 'to'),
            ('CC', 'cc', 'cc'),
            ('IN', 'in', 'in'),
            ('FW', 'fw', 'fw'),
            ('INIT', 'init','init'),
            ('EMPTY','empty','empty')
        ]
    )
 
    def execute(self, context):
        if self.action == 'TAG':
            self.seperate_by_loose_parts(context=context)
        elif self.action == 'INIT':
            self.init_building_selected_bld() 
        elif self.action == 'INIT_WORLD':
            self.coplanner_seperate(context=context)
        elif self.action == 'EMPTY':
            self.set_attributes_to_empty(context=context)
        elif self.action == 'W':
            self.tag_wall(context=context)
        elif self.action == 'R':
            self.tag_roof(context=context)
        elif self.action == 'FW':
            self.fill_windows_verts(context=context)
        elif self.action == 'D':
            self.tag_door(context=context)
        elif self.action == 'WI':
            self.tag_window(context=context)
        elif self.action == 'H':
            self.set_child_hirarchy_name(self)
        elif self.action == 'P':
            self.set_parent()
        elif self.action == 'S':
            self.clear_parent_hirarchy()
        elif self.action == "G":
            CityGML_Operators.add_tag_string("Ground")
        elif self.action == "T":
            CityGML_Operators.add_tag_string("Tunnel")
        elif self.action == "IN":
            CityGML_Operators.add_tag_string("Indoor")
        elif self.action == "TO":
            CityGML_Operators.add_tag_string("Opening")
            bpy.ops.object.editmode_toggle()
            bpy.ops.mesh.select_all(action='SELECT')
            bpy.ops.mesh.separate(type='LOOSE')
            bpy.ops.object.editmode_toggle()
        elif self.action == "CC":
            bpy.ops.view3d.snap_selected_to_cursor(use_offset=False)
        return {'FINISHED'}
 
    @staticmethod
    def seperate_by_loose_parts(context):
        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.mesh.separate(type='LOOSE')

            
    @staticmethod
    def set_attributes_to_empty(context):
        for i in range(10):
            bpy.ops.mesh.select_more()
        bpy.ops.mesh.separate(type='SELECTED')        
        

    @staticmethod
    def set_parent():
        bpy.ops.object.parent_set(type='OBJECT', keep_transform=True)
    
    @staticmethod
    def tag_wall(context):
        CityGML_Operators.add_tag_string("Wall")
        
    @staticmethod
    def tag_door(context):
        CityGML_Operators.add_tag_string("Door")
        
    @staticmethod
    def tag_window(context):
        CityGML_Operators.add_tag_string("Window")
        bpy.ops.object.editmode_toggle()
        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.mesh.separate(type='LOOSE')
        bpy.ops.object.editmode_toggle()
        
    @staticmethod
    def fill_windows_verts(context):
        bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='EDGE')
        bpy.ops.mesh.edge_face_add()
        bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='FACE')
        bpy.ops.mesh.separate(type='SELECTED')   
        bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='VERT')

    
    @staticmethod
    def tag_roof(context):
        CityGML_Operators.add_tag_string("Roof")
        
    @staticmethod    
    def add_tag_string(prefix):
        for object in bpy.context.selected_objects:
            object.name = prefix 
        
    @staticmethod
    def coplanner_seperate(context):
        bpy.ops.mesh.select_similar(type='COPLANAR', threshold=0.05)
        bpy.ops.mesh.separate(type='SELECTED')


    @staticmethod 
    def set_child_hirarchy_name(self):
        for obj in bpy.context.selected_objects:
            for child in self.getChildren(obj):
                self.set_parent_name2hirarchy(self, child, obj.name)
                
                
    @staticmethod
    def init_building_selected_bld():
        selected_objs = bpy.context.selected_objects 
        bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='MEDIAN')
        for obj in selected_objs:
            bpy.ops.object.empty_add(type='PLAIN_AXES', align='WORLD', location = obj.location, scale=(1,1,1))
            empty_obj = bpy.context.object 
            empty_obj.empty_display_size = 10
            empty_obj.empty_display_type = 'SPHERE'
            empty_obj.name = "bld_{}".format(obj.name).replace(".", "_")
            obj.parent =  empty_obj
            obj.matrix_parent_inverse = empty_obj.matrix_world.inverted()
               
      
    @staticmethod
    def getChildren(myObject): 
        children = [] 
        for ob in bpy.data.objects: 
            if ob.parent == myObject: 
                children.append(ob) 
        return children 

    @staticmethod
    def set_parent_name2hirarchy(self, object, name):
        object.name = object.name + "_[h]_" + name
        object.data.name =  object.name
        for child in self.getChildren(object):
            self.set_parent_name2hirarchy(self, child, object.name)
            
    def clear_parent_hirarchy(self):
        for obj in bpy.context.selected_objects:
            obj.name = obj.name.split('_[h]_')[0]
            if not obj.data is None: 
                obj.data.name = obj.name
            
            

# -------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------


# ------------------------------------------------------------------------------------------- 
def register():
    register_class(CityGML_Operators)
    register_class(CityGMLPanel)
 
 
def unregister():
    unregister_class(CityGML_Operators)
    unregister_class(CityGMLPanel)


#@staticmethod 
#def set_child_hirarchy_name():
#    for obj in bpy.context.selected_objects:
#        for child in getChildren(obj):
#            set_parent_name2hirarchy(child, obj.name)
#        
#        
#  
#@staticmethod
#def getChildren(myObject): 
#    children = [] 
#    for ob in bpy.data.objects: 
#        if ob.parent == myObject: 
#            children.append(ob) 
#    return children 

#@staticmethod
#def set_parent_name2hirarchy(object, name):
#    object.name = object.name + "_[h]_" + name
#    for child in getChildren(object):
#        set_parent_name2hirarchy(child, object.name)
#        


 
if __name__ == '__main__':
    register()
    
    
    
